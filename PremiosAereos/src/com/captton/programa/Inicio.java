package com.captton.programa;

import java.util.ArrayList;

import com.captton.empresaAereos.*;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * Crear pasajeros, pilotos, azafates
		 */
		Pasajero pas1 = new Pasajero(1, 36999888, "Niki", "Bibi");
		Pasajero pas2 = new Pasajero(2, 36998777, "�i", "Bi");
		Pasajero pas3 = new Pasajero(3, 36888777, "Nolita", "Litbi");
		Pasajero pas4 = new Pasajero(4, 99888777, "Ifer", "Nalito");
		Pasajero pas5 = new Pasajero(5, 36999877, "Panchito", "de Asis");

		Azafate az1 = new Azafate(6, 85777444, "Gigolo", "Furiante");
		Azafate az2 = new Azafate(7, 89555774, "Grigorio", "Inturiante");
		Azafate az3 = new Azafate(8, 89777444, "Gina", "Surfiante");
		Azafate az4 = new Azafate(9, 89555777, "Furiaz", "Giriante");

		Piloto pil1 = new Piloto(10, 78995511, "Honas", "Xarate");
		Piloto pil2 = new Piloto(11, 99555111, "jomas", "Jarase");
		Piloto pil3 = new Piloto(12, 78999551, "Conmazas", "Yunpepe");

		ArrayList<Persona> personas = new ArrayList<Persona>();

		/**
		 * ascender, viajar
		 */

		pas1.ascender();
		pas3.ascender();

		az1.ascender();
		az4.ascender();

		pil3.ascender();

		pas1.viajar(90);
		pas2.viajar(56);
		pas3.viajar(105);
		pas4.viajar(897);
		pas5.viajar(0);

		az1.viajar(89);
		az2.viajar(9);
		az3.viajar(56);
		az4.viajar(0);

		pil1.viajar(80);
		pil2.viajar(0);
		pil3.viajar(29);

		/**
		 * entregar premios
		 */
		personas.add(pas1);
		personas.add(pas2);
		personas.add(pas3);
		personas.add(pas4);
		personas.add(pas5);

		personas.add(az1);
		personas.add(az2);
		personas.add(az3);
		personas.add(az4);

		personas.add(pil1);
		personas.add(pil2);
		personas.add(pil3);

		for (Persona per : personas) {
			System.out.println(per.getName());
			per.recibirPremio();
		}

		/**
		 * Listar
		 */
	}

}
