package com.captton.empresaAereos;

public abstract class Persona {
	private int id;
	private int dni;
	private String name;
	private String surname;
	private int horasDeVuelo;
	public Persona(int id, int dni, String name, String surname) {
		super();
		this.id = id;
		this.dni = dni;
		this.name = name;
		this.surname = surname;
		this.horasDeVuelo = 0;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public int getHorasDeVuelo() {
		return horasDeVuelo;
	}
	public void setHorasDeVuelo(int horasDeVuelo) {
		this.horasDeVuelo = horasDeVuelo;
	}
	public Persona() {
		super();
	}
	
	public final void sumarHoras(int horas) {
		this.horasDeVuelo = horas + this.horasDeVuelo;
	}
	public abstract void viajar(int horas);
		
	
	public abstract void recibirPremio();
	
	public abstract void ascender();
	
	
	
}
