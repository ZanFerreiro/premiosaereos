package com.captton.empresaAereos;

public class Piloto extends Persona {
	private boolean esComandante;
	public Piloto(int id, int dni, String name, String surname) {
		super(id, dni, name, surname);
		esComandante = false;
		// TODO Auto-generated constructor stub
	}

	public Piloto() {
		// TODO Auto-generated constructor stub
		esComandante = false;
	}

	

	@Override
	public void recibirPremio() {
		int horasFaltan;
		// TODO Auto-generated method stub
		if(this.getHorasDeVuelo()>=30) {
			System.out.println("Felcitaciones ud. ha  llegado/superado las 30 horas de vuelo, tiene una semana de descanso");
		}else {
			horasFaltan = 30-this.getHorasDeVuelo();
			System.out.println("Aun te faltan "+horasFaltan+" cantidad de horas para el premio");
			
		}
	}

	@Override
	public void viajar(int horas) {
		// TODO Auto-generated method stub
		sumarHoras(horas);
		System.out.println("Tripulación lista para despegar");
		
	}
	
	public void ascender() {
		this.esComandante = true;
	}

}
